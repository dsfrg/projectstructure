﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs;
using Common.DTOs.QueryDtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QueryController : ControllerBase
    {
        private readonly QueryService _queryService;

        public QueryController(QueryService queryService)
        {
            _queryService = queryService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDto>> GetCombinedData()
        {
            return Ok(_queryService.GetCombinedDataStructureDto());
        }

        [HttpGet("projecttotask/{userId}")]
        public ActionResult<IEnumerable<ProjectToTaskCountStructureDto>> GetProjectWithTaskCountDictionary(int userId)
        {
            return Ok(_queryService.GetDictionaryProjectToTaskCountByUserId(userId));
        }

        [HttpGet("usertaknameless45/{userId}")]
        public ActionResult<IEnumerable<TaskDto>> GetTasksByUserIdWhereNameLessThan45(int userId)
        {
            return Ok(_queryService.GetTasksByUserIdWhereNameLessThan45(userId));
        }

        [HttpGet("finishedtasksidname/{userId}")]
        public ActionResult<IEnumerable<TasksIdNameDto>> GetFromCollectionOfTasksWhichAreFinished(int userId)
        {
            return Ok(_queryService.GetFromCollectionOfTasksWhichAreFinished(userId));
        }


        [HttpGet("teamswithusers")]
        public ActionResult<IEnumerable<TeamUsersDto>> GetTeamsOlderThan10YearsPlusUsers()
        {
            return Ok(_queryService.GetTeamsOlderThan10Years());
        }

        [HttpGet("userswithtasks")]
        public ActionResult<IEnumerable<UserTasksDto>> GetListOfUsersWithTasks()
        {
            return Ok(_queryService.GetListOfUsers());
        }

        [HttpGet("infoaboutuser/{userId}")]
        public ActionResult<CombinedInfoAboutUserTask6Dto> GetInfoAboutUser(int userId)
        {
            return Ok(_queryService.GetInfoAboutUser(userId));
        }

        [HttpGet("GetProjectInfo")]
        public ActionResult<IEnumerable<CombinedInfoAboutProjectTask7Dto>> GetProjectInfo()
        {
            return Ok(_queryService.GetProjectInfo());
        }

    }
}
