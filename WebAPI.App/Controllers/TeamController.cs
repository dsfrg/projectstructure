﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamController(TeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDto>> GetAllTeams()
        {
            return Ok(_teamService.GetTeams());
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDto> GetSingleTeam(int id)
        {
            return Ok(_teamService.GetTeam(id));
        }


        [HttpPost]
        public IActionResult AddSingleTeam(TeamDto team)
        {
            _teamService.AddTeam(team);
            return Ok();
        }

        [HttpPost("range")]
        public IActionResult AddTeams(IEnumerable<TeamDto> teams)
        {
            _teamService.AddRangeOfTeams(teams);
            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteTeam(TeamDto team)
        {
            _teamService.Delete(team);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTeamById(int id)
        {
            _teamService.DeleteById(id);
            return NoContent();
        }


        [HttpPut]
        public IActionResult ChangeTeam(TeamDto team)
        {
            _teamService.Update(team);
            return Ok();
        }
    }
}
