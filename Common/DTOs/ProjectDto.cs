﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class ProjectDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }

        public string? Description { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }

        public int AuthorId { get; set; }
        public UserDto Author { get; set; }
        public int TeamId { get; set; }
        public TeamDto Team { get; set; }
        public List<TaskDto> Tasks { get; set; }
    }
}
