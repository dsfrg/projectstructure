﻿using AutoMapper;
using Common.DTOs;
using Common.DTOs.QueryDtos;
using DAL.Entities;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Services
{
    public class QueryService
    {
        private readonly IRepository<Project> _projectRepo;
        private readonly IRepository<User> _userRepo;
        private readonly IRepository<Team> _teamRepo;
        private readonly IRepository<Task> _taskRepo;
        private readonly IMapper _mapper;


        public QueryService(
            IRepository<Project> projectRepo,
            IRepository<User> userRepo,
            IRepository<Team> teamRepo,
            IRepository<Task> taskRepo,
            IMapper mapper
            )
        {
            _projectRepo = projectRepo;
            _userRepo = userRepo;
            _teamRepo = teamRepo;
            _taskRepo = taskRepo;
            _mapper = mapper;
        }

        public IEnumerable<ProjectDto> GetCombinedDataStructureDto()
        {
            return _mapper.Map<IEnumerable<ProjectDto>>(GetCombinedDataStructure());
        }
        public IEnumerable<Project> GetCombinedDataStructure() //create a hierarchical data structure 
        {
            return _projectRepo.GetItems()
                .Join(_userRepo.GetItems(),
                project => project.AuthorId,
                author => author.Id,
                (project, author) => new Project()
                {
                    Id = project.Id,
                    Name = project.Name,
                    Description = project.Description,
                    CreatedAt = project.CreatedAt,
                    Deadline = project.Deadline,
                    AuthorId = project.AuthorId,
                    Author = author,
                    TeamId = project.TeamId,
                    Team = project.Team,
                    Tasks = project.Tasks
                })
                .Join(_teamRepo.GetItems(),
                project => project.TeamId,
                team => team.Id,
                (project, team) => new Project()
                {
                    Id = project.Id,
                    Name = project.Name,
                    Description = project.Description,
                    CreatedAt = project.CreatedAt,
                    Deadline = project.Deadline,
                    AuthorId = project.AuthorId,
                    Author = project.Author,
                    TeamId = project.TeamId,
                    Team = team,
                    Tasks = project.Tasks
                })
                .GroupJoin(_taskRepo.GetItems(),
                project => project.Id,
                task => task.ProjectId,
                (project, task) => new Project()
                {
                    Id = project.Id,
                    Name = project.Name,
                    Description = project.Description,
                    CreatedAt = project.CreatedAt,
                    Deadline = project.Deadline,
                    AuthorId = project.AuthorId,
                    Author = project.Author,
                    Tasks = task.Join(_userRepo.GetItems(),
                            task => task.PerformerId,
                            user => user.Id,
                            (task, user) => new Task()
                            {
                                Id = task.Id,
                                Name = task.Name,
                                Description = task.Description,
                                CreatedAt = task.CreatedAt,
                                FinishedAt = task.FinishedAt,
                                State = task.State,
                                ProjectId = task.ProjectId,
                                Project = _projectRepo.GetItems().FirstOrDefault(x => x.Id == task.ProjectId),
                                PerformerId = task.PerformerId,
                                Performer = user
                            }).ToList(),
                    TeamId = project.TeamId,
                    Team = project.Team
                });
        }
        public IEnumerable<ProjectToTaskCountStructureDto> GetDictionaryProjectToTaskCountByUserId(int userId)
        {

            return GetCombinedDataStructureDto().Where(p => p.AuthorId == userId)
                .Select(p => new ProjectToTaskCountStructureDto()
                { 
                    Project = p,
                    CountOfTasks = p.Tasks.Count
                });
        }

        public IEnumerable<TaskDto> GetTasksByUserIdWhereNameLessThan45(int userId)
        {
            return GetCombinedDataStructureDto().SelectMany(x => x.Tasks)
                .Where(p => p.PerformerId == userId);
        }

        public IEnumerable<TasksIdNameDto> GetFromCollectionOfTasksWhichAreFinished(int userId)
        {
            return GetCombinedDataStructureDto().SelectMany(x => x.Tasks)
                    .Where(p => p.PerformerId == userId && p.State == TaskStateDto.Finished && p.FinishedAt.Year == 2020)
                    .Select(x => new TasksIdNameDto()
                    {
                        Id = x.Id,
                        Name = x.Name
                    });
        }

        public IEnumerable<TeamUsersDto> GetTeamsOlderThan10Years()
        {
           return  GetCombinedDataStructure().Select(x => x.Team).Distinct()
            .Join(_userRepo.GetItems(),
                team => team.Id,
                user => user.TeamId,
                (team, user) => new User()
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    Birthday = user.Birthday,
                    RegisteredAt = user.RegisteredAt,
                    TeamId = user.TeamId,
                    Team = team
                })
                .OrderByDescending(x => x.RegisteredAt)
                .GroupBy(x => x.Team)
                .Where(x => x.All(p => (DateTime.Now.Year - p.Birthday.Year) > 10))
                .Select(x => new TeamUsersDto()
                {
                    TeamId = x.Key.Id,
                    TeamName = x.Key.Name,
                    Users = _mapper.Map<IEnumerable<UserDto>>(x.ToList())
                });
        }

        public IEnumerable<UserTasksDto> GetListOfUsers()
        {
            return GetCombinedDataStructure()
                .SelectMany(x => x.Tasks)
                .OrderBy(x => x.Performer.FirstName)
                .GroupBy(x => x.Performer)
                .Select(x => new UserTasksDto() { 
                    User = _mapper.Map<UserDto>(x.Key),
                    Tasks = _mapper.Map<IEnumerable<TaskDto>>(x.OrderByDescending(x => x.Name.Length))
                });
        }

        public CombinedInfoAboutUserTask6Dto GetInfoAboutUser(int userId)
        {
            return GetCombinedDataStructureDto().Where(x => x.AuthorId == userId)
                .Select(x => new CombinedInfoAboutUserTask6Dto()
                {
                    User = x.Author,
                    LastProject = GetCombinedDataStructureDto().ToList().Find(q => q.CreatedAt == GetCombinedDataStructureDto().Where(p => p.AuthorId == x.Author.Id)
                                    .Max(z => z.CreatedAt)),
                    CountOfTasks = _taskRepo.GetItems().Count(t => t.PerformerId == x.AuthorId),
                    CountOfUnfinishedOrCanceledTasks = _taskRepo.GetItems().Count(t => t.State != TaskState.Finished && t.PerformerId == x.AuthorId),
                    LongestTask = _mapper.Map<TaskDto>(_taskRepo.GetItems().Where(t => t.PerformerId == x.AuthorId)
                                    .OrderByDescending(t => t.FinishedAt - t.CreatedAt)
                                    .FirstOrDefault())
                })
                .FirstOrDefault();
        }

        public IEnumerable<CombinedInfoAboutProjectTask7Dto> GetProjectInfo()
        {
            return GetCombinedDataStructure().Join(
                _taskRepo.GetItems(),
                project => project.Id,
                task => task.ProjectId,
                (project, task) => new Task()
                {
                    Id = task.Id,
                    Name = task.Name,
                    Description = task.Description,
                    CreatedAt = task.CreatedAt,
                    FinishedAt = task.FinishedAt,
                    State = task.State,
                    ProjectId = task.ProjectId,
                    Project = project,
                    PerformerId = task.PerformerId,
                    Performer = _userRepo.GetItems().ToList().Find(x => x.Id == project.AuthorId)
                })
                .GroupBy(x => x.Project)
                .Select(x => new CombinedInfoAboutProjectTask7Dto()
                {
                    Project = _mapper.Map<ProjectDto>(x.Key),
                    LongestTaskByDescription = _mapper.Map<TaskDto>(_taskRepo.GetItems().Where(t => t.ProjectId == x.Key.Id).OrderBy(t => t.Description).First()),
                    ShortestTaskByName = _mapper.Map<TaskDto>(_taskRepo.GetItems().Where(t => t.ProjectId == x.Key.Id).OrderBy(t => t.Name).Last()),
                    CountOfUsers = _userRepo.GetItems().Count(u => u.TeamId == x.Key.TeamId && (_taskRepo.GetItems().Count(q => q.ProjectId == x.Key.Id) < 3 || x.Key.Description.Length > 20))
                });
        }
    }
}
