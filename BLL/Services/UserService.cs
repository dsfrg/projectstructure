﻿using AutoMapper;
using Common.DTOs;
using DAL.Entities;
using DAL.Repository;
using System.Collections.Generic;


namespace BLL.Services
{
    public class UserService
    {
        private readonly IRepository<User> _userRepo;
        private readonly IMapper _mapper;

        public UserService(IRepository<User> repo, IMapper mapper)
        {
            _userRepo = repo;
            _mapper = mapper;
        }

        public void AddUser(UserDto item)
        {
            _userRepo.Add(_mapper.Map<User>(item));
        }

        public void AddRangeOfUsers(IEnumerable<UserDto> range)
        {
            _userRepo.AddRange(_mapper.Map<IEnumerable<User>>(range));
        }

        public void Delete(UserDto item)
        {
            _userRepo.Delete(_mapper.Map<User>(item));
        }

        public void DeleteById(int id)
        {
            _userRepo.DeleteById(id);
        }

        public UserDto GetUser(int id)
        {
            return _mapper.Map<UserDto>(_userRepo.GetItem(id));
        }

        public IEnumerable<UserDto> GetUsers()
        {
            return _mapper.Map<IEnumerable<UserDto>>(_userRepo.GetItems());
        }

        public void Update(UserDto item)
        {
            _userRepo.Update(_mapper.Map<User>(item));
        }
    }
}
