﻿using DAL.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DAL.Context
{
    public class DataContext
    {
        public List<Project> Projects { get; set; }
        public List<Task> Tasks { get; set; }
        public List<Team> Teams { get; set; }
        public List<User> Users { get; set; }


        public DataContext()
        {
            Teams = new List<Team>(JsonConvert.DeserializeObject<IEnumerable<Team>>(ReadDataFromJson(@"..\Common\StarterData\Teams.json")));
            Users = new List<User>(JsonConvert.DeserializeObject<IEnumerable<User>>(ReadDataFromJson(@"..\Common\StarterData\Users.json")));
            Projects = new List<Project>(JsonConvert.DeserializeObject<IEnumerable<Project>>(ReadDataFromJson(@"..\Common\StarterData\Projects.json")));
            Tasks = new List<Task>(JsonConvert.DeserializeObject<IEnumerable<Task>>(ReadDataFromJson(@"..\Common\StarterData\Tasks.json")));
        }


        public string ReadDataFromJson(string path)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
