﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository
{
    public interface IRepository<T>
        where T: class
    {
        T GetItem(int id);
        void Add(T item);
        void AddRange(IEnumerable<T> range);
        void Update(T item);
        void Delete(T item);
        void DeleteById(int id);

        IEnumerable<T> GetItems();
    }
}
