﻿using AutoMapper;
using DAL.Context;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DAL.Repository
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly DataContext _context;
        public TeamRepository(DataContext context)
        {
            _context = context;
        }
        public void Add(Team item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var team = _context.Teams.FirstOrDefault(x => x.Id == item.Id);
            if (team != null)
            {
                var nextIndex = _context.Teams.OrderBy(x => x.Id).Last().Id;
                item.Id = ++nextIndex;
            }

            _context.Teams.Add(item);
        }

        public void AddRange(IEnumerable<Team> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach (var item in range)
            {
                if (_context.Teams.Any(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }

            _context.Teams.AddRange(range);
        }

        public void Delete(Team item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.Teams.Remove(item);
        }

        public void DeleteById(int id)
        {
            var team = _context.Teams.FirstOrDefault(x => x.Id == id);
            if (team == null)
                throw new ArgumentException("id");

            _context.Teams.Remove(team);
        }

        public Team GetItem(int id)
        {
            var team = _context.Teams.FirstOrDefault(x => x.Id == id);
            if (team == null)
                throw new ArgumentException("id");

            return team;
        }

        public IEnumerable<Team> GetItems()
        {
            return _context.Teams;
        }

        public void Update(Team item)
        {
            var team = _context.Teams.FirstOrDefault(x => x.Id == item.Id);
            if (team == null)
                throw new ArgumentException("item");
            _context.Teams[_context.Teams.IndexOf(_context.Teams.First(x => x.Id == item.Id))] = item;
        }
    }
}
