﻿using AutoMapper;
using DAL.Context;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repository
{
    public class UserRepository : IRepository<User>
    {
        private readonly DataContext _context;
        public UserRepository(DataContext context)
        {
            _context = context;
        }
        
        public void Add(User item)
        {
            if (item is null)
                throw new ArgumentNullException("item");


            var user = _context.Users.FirstOrDefault(x => x.Id == item.Id);
            if (user != null)
            {
                var nextIndex = _context.Users.OrderBy(x => x.Id).Last().Id;
                item.Id = ++nextIndex;
            }

            _context.Users.Add(item);
        }

        public void AddRange(IEnumerable<User> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach (var item in range)
            {
                if (_context.Users.Any(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }

            _context.Users.AddRange(range);
        }

        public void Delete(User item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.Users.Remove(item);
        }

        public void DeleteById(int id)
        {
            var user = _context.Users.FirstOrDefault(x => x.Id == id);
            if (user == null)
                throw new ArgumentException("user");

            _context.Users.Remove(user);
        }

        public User GetItem(int id)
        {
            var user = _context.Users.FirstOrDefault(x => x.Id == id);
            if (user == null)
                throw new ArgumentException("user");

            return user;
        }

        public IEnumerable<User> GetItems()
        {
            return _context.Users;
        }

        public void Update(User item)
        {
            var user = _context.Users.FirstOrDefault(x => x.Id == item.Id);
            if (user == null)
                throw new ArgumentException("id");
            _context.Users[_context.Users.IndexOf(_context.Users.First(x => x.Id == item.Id))] = item;
        }

        
    }
}
