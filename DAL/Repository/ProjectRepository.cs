﻿using AutoMapper;
using DAL.Context;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repository
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly DataContext _context;
        public ProjectRepository(DataContext context)
        {
            _context = context;
        }
        public void Add(Project item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var project = _context.Projects.FirstOrDefault(x => x.Id == item.Id);
            if (project != null)
            {
                var nextIndex = _context.Projects.OrderBy(x => x.Id).Last().Id;
                item.Id = ++nextIndex;
            }

            _context.Projects.Add(item);
        }

        public void AddRange(IEnumerable<Project> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach(var item in range)
            {
                if (_context.Projects.Any(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }

            _context.Projects.AddRange(range);
        }

        public void Delete(Project item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.Projects.Remove(item);
        }

        public void DeleteById(int id)
        {
            var project = _context.Projects.FirstOrDefault(x => x.Id == id);
            if (project == null)
                throw new ArgumentException("id");

            _context.Projects.Remove(project);
        }

        public Project GetItem(int id)
        {
            var project = _context.Projects.FirstOrDefault(x => x.Id == id);
            if (project == null)
                throw new ArgumentException("id");

            return project;
            
        }

        public IEnumerable<Project> GetItems()
        {
            return _context.Projects;
        }

        public void Update(Project item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var project = _context.Projects.FirstOrDefault(x => x.Id == item.Id);
            if(project is null)
                throw new ArgumentException("item");
            _context.Projects[_context.Projects.IndexOf(_context.Projects.First(x => x.Id == item.Id))] = item;
            /*project.Name = item.Name;
            project.Description = item.Description;
            project.CreatedAt = item.CreatedAt;
            project.Deadline = item.Deadline;
            project.AuthorId = item.AuthorId;
            project.Author = item.Author;
            project.TeamId = item.TeamId;
            project.Team = item.Team;
            project.Tasks = item.Tasks;*/

        }
    }
}
