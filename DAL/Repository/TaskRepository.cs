﻿using AutoMapper;
using DAL.Context;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repository
{
    public class TaskRepository : IRepository<Task>
    {
        private readonly DataContext _context;
        public TaskRepository(DataContext context)
        {
            _context = context;
        }
        public void Add(Task item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var task = _context.Tasks.FirstOrDefault(x => x.Id == item.Id);
            if (task != null)
            {
                var nextIndex = _context.Tasks.OrderBy(x => x.Id).Last().Id;
                item.Id = ++nextIndex;
            }

            _context.Tasks.Add(item);
        }

        public void AddRange(IEnumerable<Task> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach (var item in range)
            {
                if (_context.Tasks.Any(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }
            _context.Tasks.AddRange(range);
        }

        public void Delete(Task item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.Tasks.Remove(item);
        }

        public void DeleteById(int id)
        {
            var task = _context.Tasks.FirstOrDefault(x => x.Id == id);
            if (task == null)
                throw new ArgumentException("id");

            _context.Tasks.Remove(task);
        }

        public Task GetItem(int id)
        {
            var task = _context.Tasks.FirstOrDefault(x => x.Id == id);
            if (task == null)
                throw new ArgumentException("id");

            return task;
        }

        public IEnumerable<Task> GetItems()
        {
            return _context.Tasks;
        }

        public void Update(Task item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var task = _context.Tasks.FirstOrDefault(x => x.Id == item.Id);
            if (task is null)
                throw new ArgumentException("item");
            _context.Tasks[_context.Tasks.IndexOf(_context.Tasks.First(x => x.Id == item.Id))] = item;
        }
    }
}
